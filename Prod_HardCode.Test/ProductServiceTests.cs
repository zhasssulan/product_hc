﻿using Microsoft.Analytics.Interfaces;
using Microsoft.Analytics.Interfaces.Streaming;
using Microsoft.Analytics.Types.Sql;
using Microsoft.Analytics.UnitTest;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prod_HardCode.Test
{
    [TestFixture]
    public class ProductServiceTests
    {
        [Test]
        public async Task GetProductsByCategoryAndFieldsAsync_ShouldReturnFilteredProducts()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;

            using (var dbContext = new DataContext(dbContextOptions))
            {
                // Добавим тестовые данные в базу данных
                var category1Id = Guid.NewGuid();
                var category2Id = Guid.NewGuid();

                var product1 = new Product
                {
                    Name = "Product1",
                    CategoryId = category1Id,
                    FieldValues = new List<ProductFieldValue>
                {
                    new ProductFieldValue { ProductFieldId = Guid.NewGuid(), Value = "Value1" }
                }
                };

                var product2 = new Product
                {
                    Name = "Product2",
                    CategoryId = category2Id,
                    FieldValues = new List<ProductFieldValue>
                {
                    new ProductFieldValue { ProductFieldId = Guid.NewGuid(), Value = "Value2" }
                }
                };

                dbContext.Products.AddRange(product1, product2);
                dbContext.SaveChanges();

                var productService = new ProductService(dbContext, /* other dependencies */);

                // Act
                var result = await productService.GetProductsByCategoryAndFieldsAsync(category1Id, new Dictionary<Guid, string>
            {
                { product1.FieldValues.First().ProductFieldId, "Value1" }
            });

                // Assert
                NUnit.Framework.Assert.IsNotNull(result);
                Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(1, result.Count()); // Ожидаем, что будет найден только один продукт
                                                                                                 // Другие проверки...
            }
        }
    } }
