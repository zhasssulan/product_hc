using Microsoft.EntityFrameworkCore;
using Prod_HardCode.Context;
using Prod_HardCode.Interface;
using Prod_HardCode.Services;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using System.Reflection;
using Prod_HardCode.Mapper;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<DataContext>(options =>
{
    options.UseSqlServer("Server=DESKTOP-Q61IKQ5;Initial Catalog=ttt;Trusted_Connection=True;Encrypt=False");
});
builder.Services.AddScoped<ICategoryService, CategoryService>();
builder.Services.AddScoped<IProductFieldService, ProductFieldService>();
builder.Services.AddScoped<IProductFieldValueService, ProductFieldValueService>();
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddAutoMapper(typeof(CategoryProfile).Assembly);
builder.Services.AddAutoMapper(typeof(ProductProfile).Assembly, typeof(ProductFieldValueProfile).Assembly);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
