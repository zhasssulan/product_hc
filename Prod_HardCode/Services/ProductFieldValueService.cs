﻿using Prod_HardCode.Context;
using Prod_HardCode.Models.Dto;
using Prod_HardCode.Models;
using Microsoft.EntityFrameworkCore;
using Prod_HardCode.Interface;

namespace Prod_HardCode.Services
{
    public class ProductFieldValueService:IProductFieldValueService
    {
        private readonly DataContext _dbContext;
        private readonly ILogger<ProductFieldValueService> _logger;
        public ProductFieldValueService(DataContext dbContext, ILogger<ProductFieldValueService> logger)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<ProductFieldValueDto> CreateProductFieldValueAsync(ProductFieldValueDto productFieldValueDto)
        {
            _logger.LogInformation(nameof(CreateProductFieldValueAsync));
            var productFieldValue = new ProductFieldValue
            {
                ProductId = productFieldValueDto.ProductId,
                ProductFieldId = productFieldValueDto.ProductFieldId,
                Value = productFieldValueDto.Value
            };

            _dbContext.ProductFieldValues.Add(productFieldValue);
            await _dbContext.SaveChangesAsync();

            return new ProductFieldValueDto
            {
                ProductId = productFieldValue.ProductId,
                ProductFieldId = productFieldValue.ProductFieldId,
                Value = productFieldValue.Value
            };
        }

        public async Task<IEnumerable<ProductFieldValueDto>> GetAllProductFieldValuesAsync()
        {
            _logger.LogInformation(nameof(GetAllProductFieldValuesAsync));
            var productFieldValues = await _dbContext.ProductFieldValues
                .Select(pf => new ProductFieldValueDto
                {
                    ProductId = pf.ProductId,
                    ProductFieldId = pf.ProductFieldId,
                    Value = pf.Value
                })
                .ToListAsync();

            return productFieldValues;
        }

        public async Task<ProductFieldValueDto> GetProductFieldValueByIdAsync(Guid id)
        {
            _logger.LogInformation(nameof(GetProductFieldValueByIdAsync));
            var productFieldValue = await _dbContext.ProductFieldValues
                .Where(pf => pf.Id == id)
                .Select(pf => new ProductFieldValueDto
                {
                    ProductId = pf.ProductId,
                    ProductFieldId = pf.ProductFieldId,
                    Value = pf.Value
                })
                .FirstOrDefaultAsync();

            return productFieldValue;
        }
    }
}
