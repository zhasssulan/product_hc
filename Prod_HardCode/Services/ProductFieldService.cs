﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Prod_HardCode.Context;
using Prod_HardCode.Interface;
using Prod_HardCode.Models;
using Prod_HardCode.Models.Dto;

namespace Prod_HardCode.Services
{
    public class ProductFieldService : IProductFieldService
    {
        private readonly DataContext _dbContext;
        private readonly ILogger<ProductFieldService> _logger;
        public ProductFieldService(DataContext dbContext, ILogger<ProductFieldService> logger)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<ProductField> CreateProductAsync(ProductFieldDto productFieldDto)
        {
            _logger.LogInformation(nameof(CreateProductAsync));
            var productField = new ProductField
            {
                Name = productFieldDto.Name,
                Type = productFieldDto.Type
            };

            _dbContext.ProductFields.Add(productField);
            await _dbContext.SaveChangesAsync();

            return productField;
        }

        public async Task<IEnumerable<ProductFieldResponseDto>> GetAllProductFieldsAsync()
        {
            _logger.LogInformation(nameof(GetAllProductFieldsAsync));
            var productFields = await _dbContext.ProductFields.ToListAsync();

            return productFields.Select(pf => new ProductFieldResponseDto
            {
                Id = pf.Id,
                Name = pf.Name,
                Type = pf.Type
            });
        }

        public async Task<ProductFieldDto> GetProductFieldAsync(Guid id)
        {
            _logger.LogInformation(nameof(GetProductFieldAsync));
            var productField = await _dbContext.ProductFields
                .FirstOrDefaultAsync(pf => pf.Id == id);
            var productFieldDto = new ProductFieldDto
            {
                Name = productField.Name,
                Type = productField.Type
            };

            return productFieldDto;
        }

    }
}

