﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Prod_HardCode.Context;
using Prod_HardCode.Interface;
using Prod_HardCode.Models;
using Prod_HardCode.Models.Dto;
using Serilog.Core;

namespace Prod_HardCode.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly DataContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<CategoryService> _logger;
        public CategoryService(DataContext dbContext, IMapper mapper, ILogger<CategoryService> logger)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task<CategoryDto> AddCategoryAsync(CategoryDto categoryCreateDTO)
        {
            _logger.LogInformation(nameof(AddCategoryAsync));
            var category = _mapper.Map<Category>(categoryCreateDTO);

            category.Id = Guid.Empty;

            _dbContext.Categories.Add(category);
            await _dbContext.SaveChangesAsync();

            return _mapper.Map<CategoryDto>(category);
        }


        public async Task<bool> DeleteCategoryAsync(Guid categoryId)
        {
            _logger.LogInformation($"DeleteCategoryAsync");
            var category = await _dbContext.Categories.FindAsync(categoryId);

            if (category == null)
            {
                _logger.LogInformation("category is not delete");
                return false;
            }

            _dbContext.Categories.Remove(category);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<CategoryDto>> GetCategoriesAsync()
        {
            _logger.LogInformation(nameof(GetCategoriesAsync));
            var categories = await _dbContext.Categories.ToListAsync();
            return _mapper.Map<List<CategoryDto>>(categories);
        }
    }
}
