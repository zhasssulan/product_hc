﻿using AutoMapper;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;
using Prod_HardCode.Context;
using Prod_HardCode.Interface;
using Prod_HardCode.Models;
using Prod_HardCode.Models.Dto;


namespace Prod_HardCode.Services
{
    public class ProductService : IProductService
    {
        private readonly DataContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ILogger<ProductService> _logger;
        public ProductService(DataContext dbContext, IMapper mapper, ILogger<ProductService> logger)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<ProductDto> CreateProductAsync(ProductDto productCreateDTO)
        {
            _logger.LogInformation(nameof(CreateProductAsync));
            var product = _mapper.Map<Product>(productCreateDTO);

            _dbContext.Products.Add(product);
            await _dbContext.SaveChangesAsync();
            return _mapper.Map<ProductDto>(product);
        }

        public async Task<ProductDto> GetProductAsync(Guid productId)
        {
            _logger.LogInformation(nameof(GetProductAsync));
            var product = await _dbContext.Products
                .Include(p => p.FieldValues)
                .SingleOrDefaultAsync(p => p.Id == productId);

            return _mapper.Map<ProductDto>(product);
        }

        public async Task<IEnumerable<ProductDto>> GetProductsAsync()
        {
            _logger.LogInformation(nameof(GetProductsAsync));
            var products = await _dbContext.Products
                .Include(p => p.FieldValues)
                .ToListAsync();

            return _mapper.Map<IEnumerable<ProductDto>>(products);
        }
        public async Task<IEnumerable<ProductDto>> GetProductsByCategoryAsync(Guid categoryId)
        {
            _logger.LogInformation(nameof(GetProductsByCategoryAsync));
            var products = await _dbContext.Products
                .Include(p => p.FieldValues)
                .Where(p => p.CategoryId == categoryId)
                .ToListAsync();

            return _mapper.Map<IEnumerable<ProductDto>>(products);
        }
        public async Task<IEnumerable<ProductDto>> GetProductsByCategoryAndFieldsAsync(Guid categoryId, Dictionary<Guid, string> filterFields)
        {
            _logger.LogInformation(nameof(GetProductsByCategoryAndFieldsAsync));
            var productsQuery = _dbContext.Products
                .Where(p => p.CategoryId == categoryId)
                .Include(p => p.FieldValues)
                 .AsQueryable();


            foreach (var filterField in filterFields)
            {
                var fieldValue = filterField.Value;
                var productFieldId = filterField.Key;

                productsQuery = productsQuery
                    .Where(p => p.FieldValues.Any(fv => fv.ProductFieldId == productFieldId && fv.Value == fieldValue));
            }

            var filteredProducts = await productsQuery.ToListAsync();
            return _mapper.Map<IEnumerable<ProductDto>>(filteredProducts);
        }

        public async Task<ProductDetailsDto> GetProductDetailsAsync(Guid productId)
        {
            _logger.LogInformation($"Product details by ID: {productId}");
            var product = await _dbContext.Products
                .Include(p => p.FieldValues)
                .SingleOrDefaultAsync(p => p.Id == productId);
            if (product == null)
            {
                _logger.LogWarning($"Product not found for details: {productId}");
                return null;
            }


            var productDetailsDto = new ProductDetailsDto
            {
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                CategoryId = product.CategoryId,
                FieldValues = product.FieldValues
                    .Select(fv => new ProductFieldValueDto
                    {
                        ProductId = fv.ProductId,
                        ProductFieldId = fv.ProductFieldId,
                        Value = fv.Value
                    })
                    .ToList()
            };

            return productDetailsDto;
        }
    }
}
