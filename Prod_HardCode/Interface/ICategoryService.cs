﻿using Prod_HardCode.Models.Dto;

namespace Prod_HardCode.Interface
{
    public interface ICategoryService
    {
        Task<IEnumerable<CategoryDto>> GetCategoriesAsync();
        Task<CategoryDto> AddCategoryAsync(CategoryDto categoryCreateDTO);
        Task<bool> DeleteCategoryAsync(Guid categoryId);
       
    }
}
