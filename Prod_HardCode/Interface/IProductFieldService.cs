﻿using Prod_HardCode.Models;
using Prod_HardCode.Models.Dto;

namespace Prod_HardCode.Interface
{
    public interface IProductFieldService
    {
        Task<ProductField> CreateProductAsync(ProductFieldDto productCreateDTO);
        Task<ProductFieldDto> GetProductFieldAsync(Guid Id);
        Task<IEnumerable<ProductFieldResponseDto>> GetAllProductFieldsAsync();


    }
}
