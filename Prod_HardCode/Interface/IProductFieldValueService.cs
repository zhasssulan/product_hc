﻿using Prod_HardCode.Models.Dto;

namespace Prod_HardCode.Interface
{
    public interface IProductFieldValueService
    {
        Task<ProductFieldValueDto> CreateProductFieldValueAsync(ProductFieldValueDto productFieldValueDto);
        Task<IEnumerable<ProductFieldValueDto>> GetAllProductFieldValuesAsync();
        Task<ProductFieldValueDto> GetProductFieldValueByIdAsync(Guid id);
    }
}
