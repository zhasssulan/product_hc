﻿using Prod_HardCode.Models.Dto;

namespace Prod_HardCode.Interface
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> GetProductsAsync();
        Task<ProductDto> CreateProductAsync(ProductDto productCreateDTO);
        Task<ProductDto> GetProductAsync(Guid productId);
        Task<IEnumerable<ProductDto>> GetProductsByCategoryAsync(Guid categoryId);
        Task<IEnumerable<ProductDto>> GetProductsByCategoryAndFieldsAsync(Guid categoryId, Dictionary<Guid, string> filterFields);
        Task<ProductDetailsDto> GetProductDetailsAsync(Guid productId);
    }
}
