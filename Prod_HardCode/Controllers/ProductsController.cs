﻿using Microsoft.AspNetCore.Mvc;
using Prod_HardCode.ApiResponses;
using Prod_HardCode.Context;
using Prod_HardCode.Interface;
using Prod_HardCode.Models.Dto;
using Prod_HardCode.Services;
using System.Net;

namespace Prod_HardCode.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
       
        private readonly IProductService _productService ;
        public ProductsController(DataContext dataContext, IProductService productService)
        {
            _productService = productService ?? throw new ArgumentNullException(nameof(productService));
        }
        [HttpGet("show")]
        public async Task<IActionResult> GetProducts()
        {
            var products = await _productService.GetProductsAsync();
            return Ok(new ApiResponse(HttpStatusCode.OK, products, "Request successful"));
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateProduct([FromBody] ProductDto productCreateDTO)
        {
            var createdProduct = await _productService.CreateProductAsync(productCreateDTO);
            return Ok(new ApiResponse(HttpStatusCode.OK, createdProduct, "Product created successfully"));
        }

        [HttpGet("showById/{productId}")]
        public async Task<IActionResult> GetProduct(Guid productId)
        {
            var product = await _productService.GetProductAsync(productId);
            if (product != null)
            {
                return Ok(new ApiResponse(HttpStatusCode.OK, product, "Request successful"));
            }
            else
            {
                return NotFound(new ApiResponse(HttpStatusCode.NotFound, null, "Product not found"));
            }
        }

        [HttpGet("filterByCategoriesId/{Id}")]
        public async Task<IActionResult> GetProductByCategoryId(Guid Id)
        {
            var product = await _productService.GetProductsByCategoryAsync(Id);
            if (product != null)
            {
                return Ok(new ApiResponse(HttpStatusCode.OK, product, "Request successful"));
            }
            else
            {
                return NotFound(new ApiResponse(HttpStatusCode.NotFound, null, "Product not found"));
            }
        }

        [HttpGet("filterWithFields")]
        public async Task<IActionResult> GetFilteredProductsWithFields([FromQuery] Guid categoryId, [FromQuery] Dictionary<Guid, string> filterFields)
        {
            var filteredProducts = await _productService.GetProductsByCategoryAndFieldsAsync(categoryId, filterFields);
            return Ok(new ApiResponse(HttpStatusCode.OK, filteredProducts, "Request successful"));
        }

        [HttpGet("filterDetails/{productId}")]
        public async Task<IActionResult> GetProductDetails(Guid productId)
        {
            try
            {
                var productDetails = await _productService.GetProductDetailsAsync(productId);

                if (productDetails == null)
                {
                    return NotFound(new ApiResponse(HttpStatusCode.NotFound, null, "Product not found"));
                }

                return Ok(new ApiResponse(HttpStatusCode.OK, productDetails, "Request successful"));
            }
            catch (Exception ex)
            {
                // Обработка ошибок, логирование и т.д.
                return StatusCode(500, new ApiResponse(HttpStatusCode.InternalServerError, null, "Internal Server Error"));
            }
        }
    }
}
