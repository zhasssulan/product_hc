﻿using Microsoft.AspNetCore.Mvc;
using Prod_HardCode.ApiResponses;
using Prod_HardCode.Interface;
using Prod_HardCode.Models.Dto;
using System.Net;

namespace Prod_HardCode.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class ProductFieldValuesController : ControllerBase
    {
        private readonly IProductFieldValueService _productFieldValueService;

        public ProductFieldValuesController(IProductFieldValueService productFieldValueService)
        {
            _productFieldValueService = productFieldValueService ?? throw new ArgumentNullException(nameof(productFieldValueService));
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateProductFieldValue([FromBody] ProductFieldValueDto productFieldValueDto)
        {
            var createdProductFieldValue = await _productFieldValueService.CreateProductFieldValueAsync(productFieldValueDto);
            return Ok(new ApiResponse(HttpStatusCode.OK, createdProductFieldValue, "Product field value created successfully"));
        }

        [HttpGet("show-all")]
        public async Task<IActionResult> GetAllProductFieldValues()
        {
            var productFieldValues = await _productFieldValueService.GetAllProductFieldValuesAsync();
            return Ok(new ApiResponse(HttpStatusCode.OK, productFieldValues, "Request successful"));
        }

        [HttpGet("showById/{id}")]
        public async Task<IActionResult> GetProductFieldValueById(Guid id)
        {
            var productFieldValue = await _productFieldValueService.GetProductFieldValueByIdAsync(id);
            if (productFieldValue != null)
            {
                return Ok(new ApiResponse(HttpStatusCode.OK, productFieldValue, "Request successful"));
            }
            else
            {
                return NotFound(new ApiResponse(HttpStatusCode.NotFound, null, "Product field value not found"));
            }
        }
    }
}

