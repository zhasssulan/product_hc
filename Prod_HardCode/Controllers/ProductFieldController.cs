﻿using Microsoft.AspNetCore.Mvc;
using Prod_HardCode.ApiResponses;
using Prod_HardCode.Context;
using Prod_HardCode.Interface;
using Prod_HardCode.Models;
using Prod_HardCode.Models.Dto;
using System.Net;

namespace Prod_HardCode.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductFieldController : Controller
    {
        private readonly IProductFieldService _productFieldService;
        public ProductFieldController( IProductFieldService productFieldService)
        {
            _productFieldService = productFieldService ?? throw new ArgumentNullException(nameof(productFieldService));
        }
        [HttpPost("create")]
        public async Task<IActionResult> CreateProductField([FromBody] ProductFieldDto productField)
        {
            var createdProductField = await _productFieldService.CreateProductAsync(productField);
            return Ok(new ApiResponse(HttpStatusCode.OK, createdProductField, "Product field created successfully"));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProductField(Guid id)
        {
            var productField = await _productFieldService.GetProductFieldAsync(id);

            if (productField == null)
            {
                return NotFound(new ApiResponse(HttpStatusCode.NotFound, null, "Product field not found"));
            }

            return Ok(new ApiResponse(HttpStatusCode.OK, productField, "Request successful"));
        }

        [HttpGet("show-all")]
        public async Task<IActionResult> GetAllProductFields()
        {
            var productFields = await _productFieldService.GetAllProductFieldsAsync();
            return Ok(new ApiResponse(HttpStatusCode.OK, productFields, "Request successful"));
        }
    }
}
