﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prod_HardCode.ApiResponses;
using Prod_HardCode.Context;
using Prod_HardCode.Interface;
using Prod_HardCode.Models.Dto;
using System.Net;

namespace Prod_HardCode.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
     
        private readonly ICategoryService _categoryService;
        public CategoriesController(DataContext dataContext, ICategoryService categoryService)
        {
            _categoryService = categoryService ?? throw new ArgumentNullException(nameof(categoryService));
        }
        [HttpGet]
        [Route("show")]
        public async Task<object> GetCategories()
        {
            var categories = await _categoryService.GetCategoriesAsync();
            return new ApiResponse(HttpStatusCode.OK, categories);
        }
        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> AddCategory(CategoryDto categoryCreateDTO)
        {
            var addedCategory = await _categoryService.AddCategoryAsync(categoryCreateDTO);
            return Ok(new ApiResponse(HttpStatusCode.OK, addedCategory, "Category created successfully"));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(Guid id)
        {
            var success = await _categoryService.DeleteCategoryAsync(id);
            if (success)
            {
                return NoContent();
            }
            else
            {
                return NotFound(new ApiResponse(HttpStatusCode.NotFound, null, "Category not found"));
            }
        }
    }
}
