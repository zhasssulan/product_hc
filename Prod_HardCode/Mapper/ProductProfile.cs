﻿using AutoMapper;
using Prod_HardCode.Models.Dto;
using Prod_HardCode.Models;

namespace Prod_HardCode.Mapper
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<ProductDto, Product>()
                 .ReverseMap();


            CreateMap<ProductFieldValueDto, ProductFieldValue>()
                .ReverseMap();
        }
    }
}
