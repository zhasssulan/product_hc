﻿using AutoMapper;
using Prod_HardCode.Models.Dto;
using Prod_HardCode.Models;

namespace Prod_HardCode.Mapper
{
    public class CategoryProfile:Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryDto>().ReverseMap();
        }
    }
}
