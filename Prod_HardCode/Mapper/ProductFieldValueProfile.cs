﻿using Prod_HardCode.Models.Dto;
using Prod_HardCode.Models;
using AutoMapper;

namespace Prod_HardCode.Mapper
{
    public class ProductFieldValueProfile:Profile
    {
        public ProductFieldValueProfile()
        {
            CreateMap<ProductFieldValueDto, ProductFieldValue>()
               .ReverseMap();
        }
    }
}
