﻿using System.ComponentModel.DataAnnotations;

namespace Prod_HardCode.Models
{
    public class Category
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string? Name { get; set; }
        public virtual List<ProductField>? AdditionalFields { get; set; }
        public Category()
        {
            AdditionalFields = new List<ProductField>();
        }
    }
}
