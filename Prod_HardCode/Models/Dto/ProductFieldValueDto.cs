﻿namespace Prod_HardCode.Models.Dto
{
    public class ProductFieldValueDto
    {
        public Guid ProductId { get; set; }
        public Guid ProductFieldId { get; set; }
        public string? Value { get; set; }
    }
}
