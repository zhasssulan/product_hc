﻿using Prod_HardCode.Models.Enums;

namespace Prod_HardCode.Models.Dto
{
    public class ProductFieldDto
    {
        public string? Name { get; set; }
        public string? Type { get; set; }
    }
}
