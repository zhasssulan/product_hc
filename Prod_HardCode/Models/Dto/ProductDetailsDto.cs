﻿namespace Prod_HardCode.Models.Dto
{
    public class ProductDetailsDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Guid CategoryId { get; set; }
        public List<ProductFieldValueDto> FieldValues { get; set; }
    }
}
