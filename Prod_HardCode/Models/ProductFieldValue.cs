﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Prod_HardCode.Models
{
    public class ProductFieldValue
    {
        [Key]
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public Guid ProductFieldId { get; set; }
        public string? Value { get; set; }

        [ForeignKey(nameof(ProductId))]
        public virtual Product? Products { get; set; }
        [ForeignKey(nameof(ProductFieldId))]
        public virtual ProductField? ProductFields { get; set; }
    }
}
