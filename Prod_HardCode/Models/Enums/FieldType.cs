﻿namespace Prod_HardCode.Models.Enums
{
    public enum FieldType
    {
        Text,
        Number,
        Boolean,
    }
}
