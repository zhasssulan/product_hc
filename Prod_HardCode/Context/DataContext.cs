﻿using Microsoft.EntityFrameworkCore;
using Prod_HardCode.Models;
using Prod_HardCode.Models.Enums;

namespace Prod_HardCode.Context
{
    public class DataContext : DbContext
    {
        public DataContext() { }

        public DataContext(DbContextOptions<DataContext> options)
        : base(options) { }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductField> ProductFields { get; set; }
        public DbSet<ProductFieldValue> ProductFieldValues { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-Q61IKQ5;Initial Catalog=ttt;Trusted_Connection=True;Encrypt=False");
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductFieldValue>()
                .HasOne(pfv => pfv.Products)  
                .WithMany(p => p.FieldValues)
                .HasForeignKey(pfv => pfv.ProductId);

            modelBuilder.Entity<ProductFieldValue>()
                .Ignore(pfv => pfv.ProductId);

            modelBuilder.Entity<Product>()
                .Property(p => p.Price)
                .HasColumnType("decimal(18, 2)");
            modelBuilder.Entity<ProductField>().ToTable("ProductFields");

          
            base.OnModelCreating(modelBuilder);
        }

    }
}
